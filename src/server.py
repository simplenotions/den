#!/usr/bin/env python3

import logging
logging.basicConfig(level=logging.INFO)
from datetime import datetime, timezone
import socket
import time

from stream import GStreamerThread
from flask import Flask, Response, render_template, request, jsonify
app = Flask(__name__)
app.gstreamer_thread = ''
app.active_count = 0

@app.route('/')
def index():
    app.active_count += 1
    if app.gstreamer_thread.running:
        logging.info('thread is running')
    else:
        logging.info('resuming stream')
        app.gstreamer_thread.resume()
        time.sleep(2)
    return render_template('index.html')

@app.route('/pause', methods=['POST','GET'])
def pause_feed():
    app.active_count -= 1
    if app.active_count < 1:
        logging.info('shutting down stream')
        app.gstreamer_thread.pause()
    else:
        logging.info('remaining viewers: {}'.format(app.active_count))
    return 'ok'

@app.route('/live')
def livestream():
    utcnow = datetime.now(timezone.utc).strftime("%Y-%m-%d'T'%H:%M:%SZ")
    video_stream = socket.socket()
    video_stream.connect(('127.0.0.1', 9001))
    def stream_video(vs):
        while True:
            yield vs.recv(2048)
    return Response(
            stream_video(video_stream),
            status=200,
            headers={
                'Date': utcnow,
                'Connection': 'close',
                'Cache-Control': 'private',
                'Content-Type': 'video/webm',
                'Server': 'DenStreamer/0.0.1'
            })

if __name__ == '__main__':
    app.gstreamer_thread = GStreamerThread()
    app.gstreamer_thread.start()
    app.run(host='0.0.0.0', port=8080, threaded=True)
