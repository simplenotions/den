#!/usr/bin/env python3

from threading import Thread
import time
import logging
logging.basicConfig(level=logging.INFO)

import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GObject, GLib

class GStreamerThread(Thread):
    def __init__(self, **kwargs):
        Thread.__init__(self)
        Gst.init(None)
        self.pipeline = None
        self.running = False
        self.launch_str = (
            'webmmux streamable=true name=avmux '
            '! tcpserversink host=127.0.0.1 port=9001  '
            'v4l2src device=/dev/video0 '
            '! video/x-raw,width=640,height=360,framerate=30/1 '
            '! queue '
            '! videoconvert '
            '! vp8enc threads=4 cpu-used=4 deadline=1 target-bitrate=1024000 '
            '! queue ! avmux.  '
            'alsasrc device=hw:2 '
            '! queue '
            '! audioconvert '
            '! vorbisenc '
            '! avmux.  '
        )

    def run(self):
        logging.info('starting pipeline')

    def resume(self):
        if self.running:
            pass
        else:
            self.running = True
            self.pipeline = Gst.parse_launch(self.launch_str)
            self.pipeline.set_state(Gst.State.PLAYING)

    def pause(self):
        self.running = False
        self.pipeline.set_state(Gst.State.NULL)
        self.pipelne = None
