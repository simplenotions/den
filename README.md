# Den

Den provides a web accessible live stream of a camera source attached to the device it's running on.

## Setting Up Den

Clone the Den repository and run the setup script to download the requirments and setup the software to run on start up.

    $ git clone https://gitlab.com/simplenotions/den.git
    $ cd den
    $ ./setup-rpi

Your stream should be available on port 8080 in a web browser (http://yourpihostname:8080). Due to the limitations of the Pi device it'll take a few seconds to load the stream for viewing but then should be ready to go. The video is muted by default to allow the auto play to work.
